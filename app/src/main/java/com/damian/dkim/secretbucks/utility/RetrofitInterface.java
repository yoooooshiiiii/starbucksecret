package com.damian.dkim.secretbucks.utility;

/**
 * Created by dkim on 10/30/2016.
 */

import com.damian.dkim.secretbucks.model.Drink;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitInterface {

    @GET("api/coffee")
    Call<List<Drink>> getDrink();

    @GET("api/coffee/search")
    Call<List<Drink>> getSearch(@Query("search") String search);

    @GET("api/coffee/category")
    Call<List<Drink>> getCategory(@Query("category") String category);


    @GET("api/coffee/featured")
    Call<Drink> getFeatured();

    @GET("api/coffee/popular")
    Call<List<Drink>> getPopular(@Query("number") int number);

    @GET("api/coffee/recent")
    Call<List<Drink>> getRecent(@Query("number") int number);

    @GET("api/coffee/detail/{id}")
    Call<Drink> getDrinkDetail(@Path("id") String id);

    @GET("api/coffee/voted/{id}")
    Call<List<Drink>> getLikedDrink(@Path("id") String id);

    @PUT("api/coffee/vote/{id}")
    Call<Drink> putVote(
            @Path("id") String id, @Query("userId") String vote);

    @FormUrlEncoded
    @PUT("api/coffee/comment/{id}")
    Call<Drink> putComment(
            @Path("id") String id, @Field("userName") String userName, @Field("userId") String userId, @Field("comment") String comment, @Field("url") String url);

    @Multipart
    @POST("api/uploadDrinkImage")
    Call<String> uploadImage(@Part MultipartBody.Part file);

}