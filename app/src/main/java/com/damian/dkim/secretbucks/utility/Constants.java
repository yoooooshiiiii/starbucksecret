package com.damian.dkim.secretbucks.utility;

/**
 * Created by dkim on 10/29/2016.
 */

public class Constants {

    public static final String COLOR_BASE = "#61ffffff";
    public static final String COLOR_USED =  "#ffffff";
    public static final String COLOR_PRIMARY = "#755C3B";
    public static final int PAGE_COUNT = 4;
    public static final String BASE_URL = "https://starbuckssecret.herokuapp.com/";
    public static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    public static final String TOKEN = "STARBUCKS";
    public static final String GOOGLEAD = "ca-app-pub-7836805723324562~9250611137";

}