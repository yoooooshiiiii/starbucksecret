package com.damian.dkim.secretbucks.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhargavms.dotloader.DotLoader;
import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.adapter.FeatureAdapter;
import com.damian.dkim.secretbucks.model.Drink;
import com.damian.dkim.secretbucks.utility.Constants;
import com.damian.dkim.secretbucks.utility.MyApplication;
import com.damian.dkim.secretbucks.utility.RetrofitInterface;
import com.damian.dkim.secretbucks.utility.ServiceGenerator;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeatureListActivity extends AppCompatActivity {
    private DotLoader spinner;
    private RecyclerView recyclerView;
    private GridLayoutManager mLayoutManager;
    private FeatureAdapter adapter;
    private List<Drink> drinkList;
    private RetrofitInterface DrinkAPI;
    private LinearLayout progressScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feature_list);

        Intent i = getIntent();
        String title = i.getStringExtra("Feature");

        progressScreen = (LinearLayout) findViewById(R.id.progress_container);
        progressScreen.setVisibility(View.VISIBLE);
        spinner = (DotLoader) this.findViewById(R.id.progressbar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView titleText = (TextView) toolbar.findViewById(R.id.toolbar_title);
        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(), "Freight Sans Black.otf");
        titleText.setText(title);
        titleText.setTypeface(face);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

        //init adview
        if(((MyApplication) getApplication()).displayAd) {
            MobileAds.initialize(getApplicationContext(), Constants.GOOGLEAD);
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);

            CoordinatorLayout mCoordinateLayout = (CoordinatorLayout) findViewById(R.id.cl_content);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    layoutParams.setMargins(0, 0, 0, mAdView.getHeight());
                    mCoordinateLayout.setLayoutParams(layoutParams);
                }
            });
        }

        //init API
        DrinkAPI = ServiceGenerator.createService(RetrofitInterface.class, Constants.TOKEN);
        drinkList = new ArrayList<>();

        if (title.equals("Popular Drinks")) {
            getPopular();
        } else if (title.equals("New Drinks")) {
            getRecent();
        }

        recyclerView = (RecyclerView) this.findViewById(R.id.rv_list);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(this, R.dimen.item_offset);
        recyclerView.addItemDecoration(itemDecoration);
        mLayoutManager = new GridLayoutManager(FeatureListActivity.this,2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //initializing adapter and attach to the recycler view
        adapter = new FeatureAdapter(this, drinkList);
        recyclerView.setAdapter(adapter);
    }

    private void getPopular() {
        Call<List<Drink>> call = DrinkAPI.getPopular(20);
        call.enqueue(new Callback<List<Drink>>() {
            @Override
            public void onResponse(Call<List<Drink>> call, Response<List<Drink>> response) {
                if (response.body() != null) {
                    drinkList.addAll(response.body());
                    adapter.notifyDataSetChanged();
                    progressScreen.setVisibility(View.GONE);
                } else {
                    progressScreen.setVisibility(View.GONE);
                    Snackbar snackbar = Snackbar
                            .make(getWindow().getDecorView().getRootView(), "Server is offline, please try again!", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", view -> getPopular());
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<List<Drink>> call, Throwable t) {
                getPopular();
            }
        });
    }

    private void getRecent() {
        Call<List<Drink>> call = DrinkAPI.getRecent(20);
        call.enqueue(new Callback<List<Drink>>() {
            @Override
            public void onResponse(Call<List<Drink>> call, Response<List<Drink>> response) {
                if (response.body() != null) {
                    drinkList.addAll(response.body());
                    adapter.notifyDataSetChanged();
                    progressScreen.setVisibility(View.GONE);
                } else {
                    progressScreen.setVisibility(View.GONE);
                    Snackbar snackbar = Snackbar
                            .make(getWindow().getDecorView().getRootView(), "Server is offline, please try again!", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", view -> getRecent());
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<List<Drink>> call, Throwable t) {
                getRecent();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
    }
}
