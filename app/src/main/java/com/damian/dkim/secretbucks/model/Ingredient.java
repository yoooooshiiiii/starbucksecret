package com.damian.dkim.secretbucks.model;

/**
 * Created by dkim on 6/7/2017.
 */

public class Ingredient {
    private String item;
    private double serving;
    private String unit;
    private double calorie;

    public Ingredient() {
    }

    public Ingredient(String item, double serving, String unit, double calorie) {
        this.item = item;
        this.serving = serving;
        this.unit = unit;
        this.calorie = calorie;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public double getServing() {
        return serving;
    }

    public void setServing(double serving) {
        this.serving = serving;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getCalorie() {
        return calorie;
    }

    public void setCalorie(double calorie) {
        this.calorie = calorie;
    }
}

