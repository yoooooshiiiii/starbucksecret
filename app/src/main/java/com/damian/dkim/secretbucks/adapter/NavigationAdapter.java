package com.damian.dkim.secretbucks.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.ui.activities.MainActivity;
import com.damian.dkim.secretbucks.ui.activities.SettingActivity;

/**
 * Created by dkim on 12/6/2016.
 */

public class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.MyViewHolder> {
    private String[] navTitles = new String[]{"HOME" ,"SEARCH", "MENU", "UPLOAD","FAVORITES","SETTINGS"};
    private int[] imageArray = new int[]{R.drawable.ic_navhome, R.drawable.ic_navsearch, R.drawable.ic_navmenu, R.drawable.ic_navupload, R.drawable.ic_navfavorite, R.drawable.ic_navsetting};
    public static final int HEADER = 0;
    public static final int LOGIN = 1;
    public static final int MENU = 2;
    Typeface face;
    Context ctx;


    public NavigationAdapter(Context ctx) {
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        if (viewType == HEADER) {
//            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_navheader, parent, false);
//            MyViewHolder vhHeader = new MyViewHolder(v);
//            return vhHeader;
////        } else if (viewType == LOGIN) {
////            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_navlogin, parent, false);
////            MyViewHolder vhLogin = new MyViewHolder(v);
////            return vhLogin;
//        } else if (viewType == MENU) {
//            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_navmenu, parent, false);
//            MyViewHolder vhMenu = new MyViewHolder(v);
//            return vhMenu;
//        }
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_navmenu, parent, false);
        DisplayMetrics displayMetrics = ctx.getResources().getDisplayMetrics();
        int deviceHeight = displayMetrics.heightPixels;
        deviceHeight = deviceHeight - (int) (25 * ctx.getResources().getDisplayMetrics().density);
        ViewGroup.LayoutParams params = v.getLayoutParams();
        params.height = deviceHeight/6;
        params.width = deviceHeight/6;
        v.setLayoutParams(params);
        MyViewHolder rv = new MyViewHolder(v);
        return rv;
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//        if (position == 0) {
////            face = Typeface.createFromAsset(ctx.getAssets(), "Playball.ttf");
////            holder.navHeader.setTypeface(face);
////            holder.navSub.setTypeface(face);
//        } else if (position > 0) {
            holder.navMenu.setText(navTitles[position]);
            holder.navIcon.setImageResource(imageArray[position]);
            holder.navClick.setOnClickListener(v -> {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                MainActivity main = (MainActivity) v.getContext();
                Fragment fragment = null;
                TabLayout tb = (TabLayout) activity.findViewById(R.id.tl_home);
                DrawerLayout mDrawerLayout = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
                switch (position) {
                    case 0:
                        TabLayout.Tab tab = tb.getTabAt(0);
                        tab.select();
                        mDrawerLayout.closeDrawers();
                        break;
                    case 1:
                        Menu menu = main.getMenu();
                        MenuItem item = menu.findItem(R.id.action_search);
                        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
                        item.expandActionView();
                        mDrawerLayout.closeDrawers();
                        break;
                    case 2:
                        TabLayout.Tab tab1 = tb.getTabAt(1);
                        tab1.select();
                        mDrawerLayout.closeDrawers();
                        break;
                    case 3:
                        Toast.makeText(ctx, "Uploading is unavailable at this time", Toast.LENGTH_LONG).show();
                        mDrawerLayout.closeDrawers();
                        break;
                    case 4:
                        TabLayout.Tab tab2 = tb.getTabAt(2);
                        tab2.select();
                        mDrawerLayout.closeDrawers();
                        break;
                    case 5:
                        Intent i = new Intent(ctx, SettingActivity.class);
                        mDrawerLayout.closeDrawers();
                        ctx.startActivity(i);
                        break;
                    default:
                        break;
                }
            });
//        }

    }


    @Override
    public int getItemCount() {
        return navTitles.length;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType;
        if (position == 0) {
            viewType = HEADER;
//        } else if (position == 1) {
//            viewType = LOGIN;
        } else {
            viewType = MENU;
        }
        return viewType;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView navMenu;
        CardView navClick;
        ImageView navIcon;

        public MyViewHolder(View v) {
            super(v);
            itemView.setOnClickListener(this);
            navMenu = (TextView) v.findViewById(R.id.tv_navmenu);
            navClick = (CardView) v.findViewById(R.id.cv_nav);
            navIcon = (ImageView) v.findViewById(R.id.iv_navicon);
        }

        @Override
        public void onClick(View v) {

        }
    }
}

