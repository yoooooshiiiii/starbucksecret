package com.damian.dkim.secretbucks.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.damian.dkim.secretbucks.model.FavIngredient;
import com.damian.dkim.secretbucks.model.Favorite;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by dkim on 10/8/2016.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "Favdb";
    private static final int DATABASE_VERSION = 4;

    /**
     * The data access object used to interact with the Sqlite database to do C.R.U.D operations.
     */

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
                /**
                 * R.raw.ormlite_config is a reference to the ormlite_config.txt file in the
                 * /res/raw/ directory of this project
                 * */
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            /**
             * creates the Todo database table
             */
            TableUtils.createTable(connectionSource, Favorite.class);
            TableUtils.createTable(connectionSource, FavIngredient.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            /**
             * Recreates the database when onUpgrade is called by the framework
             */
            TableUtils.dropTable(connectionSource, Favorite.class, false);
            TableUtils.dropTable(connectionSource, FavIngredient.class, false);
            onCreate(database, connectionSource);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    private Dao<Favorite, Integer> favDao = null;
    private Dao<FavIngredient, Integer> detailDao = null;
    public Dao<Favorite, Integer> getFavDataDao() {
        if (null == favDao) {
            try {
                favDao = getDao(Favorite.class);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        return favDao;
    }

    public Dao<FavIngredient, Integer> getDetailDao() {
        if (null == detailDao) {
            try {
                detailDao = getDao(FavIngredient.class);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        return detailDao;
    }


}