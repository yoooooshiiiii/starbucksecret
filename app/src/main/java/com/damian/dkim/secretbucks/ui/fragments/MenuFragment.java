package com.damian.dkim.secretbucks.ui.fragments;


import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.adapter.MenuAdapter;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {

    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;
    public MenuAdapter adapter;
    private List<String> menuList;
    private int layoutHeight;
    private LinearLayout mRelativeLayout;


    public MenuFragment() {
        // Required empty public constructor
    }
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if(isVisibleToUser && isResumed()){
//            FabUtil.hideFab(fab);
//            FabUtil.showFab(fab2);
//            System.out.println("menu frag");
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_menu, container, false);
//        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
//        fab2 = (FloatingActionButton) getActivity().findViewById(R.id.fab2);
        recyclerView = (RecyclerView) v.findViewById(R.id.rv_menu);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        //disable hardware acceleration to enable dash
        v.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        //create paint object defining divider detail
        Paint paint = new Paint();
        paint.setColor(getResources().getColor(R.color.colorPrimary));
        paint.setAntiAlias(true);

        //draw dividers
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .paint(paint)
                .marginResId(R.dimen.menudivider_margin, R.dimen.menudivider_margin)
                .build());
//        recyclerView.addItemDecoration(new VerticalDividerItemDecoration.Builder(getActivity())
//                .paint(paint)
//                .marginResId(R.dimen.menudivider_margin, R.dimen.menudivider_margin)
//                .showLastDivider()
//                .build());
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        menuList = new ArrayList<>();
        menuList.add("Frappuccinos");
        menuList.add("Hot Drinks");
        menuList.add("Cold Drinks");
        menuList.add("Teas");
//        menuIconList.add(R.drawable.menu1);
//        menuIconList.add(R.drawable.menu2);
//        menuIconList.add(R.drawable.menu3);
//        menuIconList.add(R.drawable.menu4);
        mRelativeLayout = (LinearLayout)v.findViewById(R.id.ll_menu);
        mRelativeLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mRelativeLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
                layoutHeight = mRelativeLayout.getHeight()-toolbar.getHeight();
                adapter = new MenuAdapter(getActivity(), menuList, layoutHeight);
                recyclerView.setAdapter(adapter);
            }
        });
        return v;
    }
}
