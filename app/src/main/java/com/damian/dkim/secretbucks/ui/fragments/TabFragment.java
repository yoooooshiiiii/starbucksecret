package com.damian.dkim.secretbucks.ui.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.utility.Constants;
import com.damian.dkim.secretbucks.utility.CustomTabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment extends Fragment {
    SharedPreferences prefs;
    CustomTabLayout t;
    ViewPager viewPager;
    public TabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tab, container, false);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //initialize tab layout
        t = (CustomTabLayout) v.findViewById(R.id.tl_home);
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        PagerAdapter pagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager(), getContext());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(t));
        t.setupWithViewPager(viewPager);
        initTabs(t);
//        t.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                viewPager.setCurrentItem(tab.getPosition());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//
//            }
//        });
        return v;
    }

    private void initTabs(TabLayout tabLayout) {
        tabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        int[] iconArray = new int[]{R.drawable.icon_home, R.drawable.icon_coffee,
                R.drawable.icon_star};
        int i = 0;
        for (int icon:iconArray){
            tabLayout.getTabAt(i).setIcon(icon);
            if(i == 0) {
                tabLayout.getTabAt(i).getIcon().setColorFilter(Color.parseColor(Constants.COLOR_USED), PorterDuff.Mode.SRC_IN);
            } else {
                tabLayout.getTabAt(i).getIcon().setColorFilter(Color.parseColor(Constants.COLOR_BASE), PorterDuff.Mode.SRC_IN);
            }
            i++;
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor(Constants.COLOR_USED), PorterDuff.Mode.SRC_IN);
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor(Constants.COLOR_BASE), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    public class PagerAdapter extends FragmentStatePagerAdapter {
        final int PAGE_COUNT = 3;
        private Context ctx;
        private String tabTitles[] = new String[] { "HOME", "MENU","FAVORITES" };

        public PagerAdapter(FragmentManager fm, Context ctx) {
            super(fm);
            this.ctx = ctx;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    HomeFragment home = new HomeFragment();
                    return home;
                case 1:
                    MenuFragment menu = new MenuFragment();
                    return menu;
                case 2:
                    FavFragment fav = new FavFragment();
                    return fav;
                default:
                    return null;
            }
        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return tabTitles[position];
//        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }
}
