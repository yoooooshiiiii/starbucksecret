package com.damian.dkim.secretbucks.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.model.Comment;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Date;
import java.util.List;

/**
 * Created by dkim on 11/9/2016.
 */

public class CommentAdapter extends RecyclerView.Adapter<MyViewHolder>{
    private Context mContext;
    private Comment comment;
    private List<Comment> commentList;

    public CommentAdapter(Context mContext, List<Comment> commentList) {
        this.mContext = mContext;
        this.commentList = commentList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_comment, parent, false);
        MyViewHolder rv = new MyViewHolder(v);
        return rv;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        //if username is null, set guestname to Guest
        comment = commentList.get(position);
        if (comment.getUserName() == null || comment.getUserName() == ""){
            comment.setUserName("Guest");
        }
        holder.comment.setText(spanComment(comment.getUserName(), comment.getComment()));

        //get uploaded image
        Picasso.with(mContext).cancelRequest(holder.commentUploadedImage);
        if (comment.getUrl() != null){
            Picasso.with(mContext).load(comment.getUrl()).resize(dpToPx(mContext,140), dpToPx(mContext,140)).centerCrop().into(holder.commentUploadedImage);
        } else {
            holder.commentUploadedImage.setImageDrawable(null);
        }

        //handles time logic
        Date date = comment.getTimeStamp();
        Date date2 = new DateTime(DateTimeZone.UTC).toDate();
        long difference = (date2.getTime() - date.getTime())/60000;
        String timeAgo;

        if (difference < 60) {
            timeAgo = "Now";
        } else if (difference < 2530) {
            timeAgo = "Recent";
        } else if (difference < 43200) {
            timeAgo = (difference/1440) + " days ago";
        } else if (difference < 86400) {
            timeAgo = "About 1 month ago";
        } else if (difference < 525600) {
            timeAgo = "About " + (difference/43200) + " months ago";
        } else
            timeAgo = "More than a year ago";

        holder.timeStamp.setText(timeAgo);

        //get profile icon
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(comment.getUserId());
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(comment.getUserName().substring(0,1), color);
        holder.commentImage.setImageDrawable(drawable);
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public SpannableStringBuilder spanComment(String str, String str2){

        SpannableStringBuilder builder = new SpannableStringBuilder();
        if (str.equals("") || str == null || str2.equals("") || str2 == null) {
            str = "Undefined";
            str2 = " ";
        }
        builder.append(str).append(" ");
        builder.setSpan(new StyleSpan(Typeface.BOLD), str.indexOf(str), str.indexOf(str)+str.length(), 0);
        builder.append(str2);
        return builder;
    }

    @Override
    public void registerAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        super.registerAdapterDataObserver(observer);
    }

    @Override
    public void unregisterAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        super.unregisterAdapterDataObserver(observer);
    }

    public void update(List<Comment> cl){
        commentList = cl;
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
}

