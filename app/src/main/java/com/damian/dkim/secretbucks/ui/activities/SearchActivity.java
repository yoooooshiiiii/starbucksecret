package com.damian.dkim.secretbucks.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhargavms.dotloader.DotLoader;
import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.adapter.ListAdapter;
import com.damian.dkim.secretbucks.model.Drink;
import com.damian.dkim.secretbucks.utility.Constants;
import com.damian.dkim.secretbucks.utility.MyApplication;
import com.damian.dkim.secretbucks.utility.RetrofitInterface;
import com.damian.dkim.secretbucks.utility.ServiceGenerator;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchActivity extends AppCompatActivity {

    private List<Drink> drinkList;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    RetrofitInterface API;
    private DotLoader spinner;
    private ListAdapter adapter;
    private String title;
    private Intent myIntent;
    private LinearLayout progressScreen;
    CoordinatorLayout mCoordinateLayout;
    private LinearLayout mLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        myIntent = getIntent();
        title = myIntent.getStringExtra("Search");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView titleText = (TextView) toolbar.findViewById(R.id.toolbar_title);
        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(), "Freight Sans Black.otf");
        titleText.setText(title);
        titleText.setTypeface(face);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);


        //loading page
        progressScreen = (LinearLayout) findViewById(R.id.progress_container);
        progressScreen.setVisibility(View.VISIBLE);
        spinner = (DotLoader) this.findViewById(R.id.text_dot_loader);

        //no result page
        mLinearLayout = (LinearLayout) findViewById(R.id.ll_noResult);
        mLinearLayout.setVisibility(View.GONE);

        if(((MyApplication) getApplication()).displayAd) {
            MobileAds.initialize(getApplicationContext(), Constants.GOOGLEAD);
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);

            CoordinatorLayout mCoordinateLayout = (CoordinatorLayout) findViewById(R.id.cl_content);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    layoutParams.setMargins(0, 0, 0, mAdView.getHeight());
                    mCoordinateLayout.setLayoutParams(layoutParams);
                }
            });
        }

        API = ServiceGenerator.createService(RetrofitInterface.class, Constants.TOKEN);

        mRecyclerView = (RecyclerView) this.findViewById(R.id.rv_list);
        mLayoutManager = new LinearLayoutManager(SearchActivity.this);
        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .marginResId(R.dimen.menudivider_margin, R.dimen.menudivider_margin)
                .build());
        mRecyclerView.setLayoutManager(mLayoutManager);
        drinkList = new ArrayList<>();
        adapter = new ListAdapter(this, drinkList);
        mRecyclerView.setAdapter(adapter);
        getDrinkList();


    }

    //get drinklist with query. ignores space in the string for reliable search result
    private void getDrinkList() {
        Call<List<Drink>> call = API.getSearch(title.replaceAll("\\s",""));
        call.enqueue(new Callback<List<Drink>>() {
            @Override
            public void onResponse(Call<List<Drink>> call, Response<List<Drink>> response) {
                if (response.body() == null || response.body().size() == 0) {
                    mLinearLayout.setVisibility(View.VISIBLE);
                    progressScreen.setVisibility(View.GONE);
                } else {
                    drinkList = response.body();
                    adapter.update(drinkList);
                    adapter.notifyDataSetChanged();
                    progressScreen.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<List<Drink>> call, Throwable t) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
