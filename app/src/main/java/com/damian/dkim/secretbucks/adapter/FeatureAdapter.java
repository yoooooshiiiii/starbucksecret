package com.damian.dkim.secretbucks.adapter;

/**
 * Created by dkim on 11/3/2016.
 */


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.model.Drink;
import com.damian.dkim.secretbucks.ui.activities.DetailActivity;
import com.damian.dkim.secretbucks.utility.MyApplication;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class FeatureAdapter extends RecyclerView.Adapter<FeatureAdapter.MyViewHolder> {

    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_ITEM = 1;
    private Context mContext;
    private List<Drink> drinkList;
    private Typeface face;
//    private final View header;
    private Drink drink;



    public FeatureAdapter(Context mContext, List<Drink> drinkList) {
//        if (header == null) {
//            throw new IllegalArgumentException("header may not be null");
//        }
//        this.header = header;
        this.mContext = mContext;
        this.drinkList = drinkList;
    }

    @Override
    public int getItemCount() {
        return drinkList.size();
    }

//    public boolean isHeader(int position) {
//        return position == 0;
//    }


    @Override
    public int getItemViewType(int position) {
        int viewType;
        if (drinkList.isEmpty()){
            viewType = VIEW_TYPE_EMPTY;
        } else {
            viewType = VIEW_TYPE_ITEM;
        }
        return viewType;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        if (viewType == ITEM_VIEW_TYPE_HEADER) {
//            return new MyViewHolder(header);
//        }
        View v;
        MyViewHolder rv;
        switch (viewType) {
            case VIEW_TYPE_ITEM:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_featurelist, parent, false);
                rv = new MyViewHolder(v);
                return rv;
            case VIEW_TYPE_EMPTY:
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_empty, parent, false);
                rv = new MyViewHolder(v);
                return rv;
            default: // default is VIEW_TYPE_ITEM
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_featurelist, parent, false);
                rv = new MyViewHolder(v);
                return rv;
        }
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//        if (!isHeader(position)) {
            drink = drinkList.get(position);
            holder.drinkName.setText(drink.getName());
            holder.drinkLike.setText(String.valueOf(drink.getVote().size()));
            holder.drinkComment.setText(String.valueOf(drink.getComment().size()));
            Picasso.with(mContext).load(drink.getUrl()).placeholder(R.drawable.placeholder).fit().centerCrop().into(holder.drinkImage);
            holder.drinkImage.setOnClickListener(v->{
                Intent i = new Intent(mContext, DetailActivity.class);
                MyApplication.getInstance().setDrink(drinkList.get(position));
                mContext.startActivity(i);});
            holder.cardLayout.setOnClickListener(v->{
                Intent i = new Intent(mContext, DetailActivity.class);
                MyApplication.getInstance().setDrink(drinkList.get(position));
                mContext.startActivity(i);
            });
    }

    public void update(List<Drink> dl){
        drinkList = dl;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView drinkName, drinkLike, drinkComment;
        CardView cardLayout;
        ImageView drinkImage;

        public MyViewHolder(View v){
            super(v);
            cardLayout = (CardView) v.findViewById(R.id.card_view);
            drinkName = (TextView)v.findViewById(R.id.title);
            drinkLike = (TextView)v.findViewById(R.id.count);
            drinkImage = (ImageView)v.findViewById(R.id.thumbnail);
            drinkComment = (TextView)v.findViewById(R.id.tv_commentCount);
        }

        @Override
        public void onClick(View v) {

        }
    }

}