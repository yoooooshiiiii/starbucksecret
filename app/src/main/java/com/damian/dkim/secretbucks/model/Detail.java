package com.damian.dkim.secretbucks.model;

/**
 * Created by dkim on 6/7/2017.
 */

public class Detail {

    private Ingredient ingredient;

    public Detail() {
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }
}
