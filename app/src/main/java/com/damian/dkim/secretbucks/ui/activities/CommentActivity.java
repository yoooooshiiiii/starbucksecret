package com.damian.dkim.secretbucks.ui.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bhargavms.dotloader.DotLoader;
import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.adapter.CommentAdapter;
import com.damian.dkim.secretbucks.model.Comment;
import com.damian.dkim.secretbucks.model.Drink;
import com.damian.dkim.secretbucks.utility.Constants;
import com.damian.dkim.secretbucks.utility.ImageCompress;
import com.damian.dkim.secretbucks.utility.MyApplication;
import com.damian.dkim.secretbucks.utility.RetrofitInterface;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.squareup.picasso.Picasso;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CommentActivity extends AppCompatActivity {
    final private static int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1010;
    final private static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1111;
    final private static int SELECT_PHOTO = 2020;
    ImageCompress compress;
    boolean imgLoaded;
    String imagePath, comment, userName, userId, url, uploadedUrl;
    Drink drink;
    List<Comment> commentList;
    RecyclerView recyclerView;
    CommentAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    EditText mEditText;
    ImageView mImageView, mImageView2;
    SharedPreferences prefs;
    int c;
    Retrofit retrofit;
    RetrofitInterface API;
    private DotLoader spinner;
    LinearLayout progressContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        //instantiating a new image compress class
        compress = new ImageCompress();

        //setup toolbar
        spinner = (DotLoader) findViewById(R.id.progressBar);
        progressContainer = (LinearLayout) findViewById(R.id.progress_container);
        progressContainer.setVisibility(View.GONE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        TextView titleText = (TextView) toolbar.findViewById(R.id.toolbar_title);
        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(), "Freight Sans Black.otf");
        titleText.setText("Comments");
        titleText.setTypeface(face);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

        //init adview
        if(((MyApplication)getApplication()).displayAd == true) {
            MobileAds.initialize(getApplicationContext(), Constants.GOOGLEAD);
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }

        //get value from shared preference into string
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        //get color from previous screen
        Intent i = getIntent();
        c = i.getIntExtra("color", 0);

        //change actionbar color to color of previous activity
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(c));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        //change status bar color
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(c);
        } else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        //get drink data from previous page
        drink = MyApplication.getInstance().getDrink();
        commentList = drink.getComment();
        //instantiating a retrofit object with baseURL
        retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API = retrofit.create(RetrofitInterface.class);
        //initializing recycler view
        recyclerView = (RecyclerView) this.findViewById(R.id.rv_comment);
        mLayoutManager = new LinearLayoutManager(CommentActivity.this);
        mLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .marginResId(R.dimen.divider_margin, R.dimen.divider_margin)
                .build());
        //initializing adapter and attach to the recycler view
        adapter = new CommentAdapter(this, commentList);
        recyclerView.setAdapter(adapter);

        //go to bottom page
        recyclerView.scrollToPosition(adapter.getItemCount() - 1);
        //handle recyclerview when keyboard is up
        recyclerView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom) -> recyclerView.scrollToPosition(adapter.getItemCount() - 1));


        //initializing editText for comment
        mEditText = (EditText) this.findViewById(R.id.et_comment);
        mEditText.setOnClickListener(v -> {
            recyclerView.scrollToPosition(adapter.getItemCount() - 1);
        });

        //initialize camera button
        mImageView = (ImageView) this.findViewById(R.id.iv_chatIcon);
        mImageView.setOnClickListener(v -> {
            // Check if image is already loaded
            if (!imgLoaded) {
                // Check permission
                if (checkReadPermission()) {
                    openGallery();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission required", Toast.LENGTH_SHORT).show();
                }
            } else if (imgLoaded) {
                //instead, show loaded image
                showImage();
            }
        });
        //initializing submit comment button
        mImageView2 = (ImageView) this.findViewById(R.id.iv_send);
        mImageView2.setOnClickListener(v -> {
            if (!mEditText.getText().toString().equals("")) {
                comment = mEditText.getText().toString();
                mEditText.setText("");
                //instantiating a Call object
                userName = prefs.getString("NICKNAME", "");
                userId = prefs.getString("ID", "");
                if (!imgLoaded) {
                    submitComment();
                } else if (imgLoaded) {
                    imageUpload();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Comment cannot be blank", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void imageUpload() {

        //create RequestBody instance
        File file = new File(imagePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        //MultipartBody.Part to send actual file name
        MultipartBody.Part body = MultipartBody.Part.createFormData("comment", file.getName(), requestFile);
        Call<String> resultCall = API.uploadImage(body);

        //Execute request async
        resultCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    uploadedUrl = response.body();
                    try {
                        file.delete();
                    } catch (Exception e) {
                        System.out.println("something wrong");
                    }
                    System.out.println(uploadedUrl);
                    submitComment();
                } else {
                    System.out.println("Fail");
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    //submitting comment
    public void submitComment() {
        Call<Drink> call = API.putComment(drink.get_id(), userName, userId, comment, uploadedUrl);
        progressContainer.setVisibility(View.VISIBLE);
        call.enqueue(new retrofit2.Callback<Drink>() {
            @Override
            public void onResponse(Call<Drink> call, Response<Drink> response) {
                getDrinkDetail();
                imagePath = null;
                imgLoaded = false;
                mImageView.setImageResource(R.drawable.ic_photooff);
                progressContainer.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Drink> call, Throwable t) {
            }
        });
    }

    public void getDrinkDetail() {
        RetrofitInterface DrinkAPI = retrofit.create(RetrofitInterface.class);
        Call<Drink> call = DrinkAPI.getDrinkDetail(drink.get_id());
        call.enqueue(new Callback<Drink>() {
            @Override
            public void onResponse(Call<Drink> call, Response<Drink> response) {
                MyApplication.getInstance().setDrink(response.body());
                drink = MyApplication.getInstance().getDrink();
                commentList = drink.getComment();
                adapter.update(commentList);
                adapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(adapter.getItemCount() - 1);
            }

            @Override
            public void onFailure(Call<Drink> call, Throwable t) {
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    private void showImage() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView mImageView3 = (ImageView) dialog.findViewById(R.id.iv_upload);
        Button mButton = (Button) dialog.findViewById(R.id.btn_ok);
        Button mButton2 = (Button) dialog.findViewById(R.id.btn_discard);
        Picasso.with(this).load(new File(imagePath)).resize(dpToPx(this, 400), dpToPx(this, 400)).transform(new RoundedTransformation(50, 4))
                .centerCrop().into(mImageView3);
        mButton.setOnClickListener(v -> dialog.dismiss());
        mButton2.setOnClickListener(v -> {
            imagePath = null;
            imgLoaded = false;
            mImageView.setImageResource(R.drawable.ic_photooff);
            dialog.dismiss();
        });
        dialog.show();
//        ImageView image = new ImageView(this);

//        AlertDialog.Builder builder =
//                new AlertDialog.Builder(this).
//                        setPositiveButton("OK", (dialog, which) -> dialog.dismiss()).
//                        setNegativeButton("DISCARD", ((dialog, which) -> {
//                            imagePath = null;
//                            imgLoaded = false;
//                            mImageView.setImageResource(R.drawable.icon_camera);
//                            dialog.dismiss();
//                        })).
//                        setView(image);
//        builder.create().show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == SELECT_PHOTO && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                imagePath = compress.compressImage(selectedImage, this);
                mImageView.setImageResource(R.drawable.ic_photo);
                imgLoaded = true;
                Toast.makeText(getApplicationContext(), "Image has been added", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    public boolean checkReadPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                return false;
            }
        } else {
            return true;
        }
    }
    public class RoundedTransformation implements com.squareup.picasso.Transformation {
        private final int radius;
        private final int margin;  // dp

        // radius is corner radii in dp
        // margin is the board in dp
        public RoundedTransformation(final int radius, final int margin) {
            this.radius = radius;
            this.margin = margin;
        }

        @Override
        public Bitmap transform(final Bitmap source) {
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

            Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            canvas.drawRoundRect(new RectF(margin, margin, source.getWidth() - margin, source.getHeight() - margin), radius, radius, paint);

            if (source != output) {
                source.recycle();
            }

            return output;
        }

        @Override
        public String key() {
            return "rounded";
        }
    }
}
