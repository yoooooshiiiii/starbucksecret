package com.damian.dkim.secretbucks.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.damian.dkim.secretbucks.BuildConfig;
import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.utility.Constants;
import com.damian.dkim.secretbucks.utility.MyApplication;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SettingActivity extends AppCompatActivity {

    private TextView mTextView, mTextView1, mTextView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        String title = "Settings";
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView titleText = (TextView) toolbar.findViewById(R.id.toolbar_title);
        Typeface face = Typeface.createFromAsset(getApplicationContext().getAssets(), "Freight Sans Black.otf");
        titleText.setText(title);
        titleText.setTypeface(face);
        titleText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);

        //initalizie textview
        mTextView = (TextView)findViewById(R.id.tv_nickname);
        mTextView1 = (TextView)findViewById(R.id.tv_uid);
        mTextView2 = (TextView) findViewById(R.id.tv_bid);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        String nickName = prefs.getString("NICKNAME",null);
        mTextView.setText(nickName);
        mTextView.setPaintFlags(mTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTextView1.setText(prefs.getString("ID",null));
        mTextView2.setText(BuildConfig.VERSION_NAME);

        mTextView.setOnClickListener(v->{
            Intent i = new Intent(this, EditActivity.class);
            startActivity(i);
        });

        if(((MyApplication) getApplication()).displayAd) {
            MobileAds.initialize(getApplicationContext(), Constants.GOOGLEAD);
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);

            CoordinatorLayout mCoordinateLayout = (CoordinatorLayout) findViewById(R.id.cl_content);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            mAdView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    layoutParams.setMargins(0, 0, 0, mAdView.getHeight());
                    mCoordinateLayout.setLayoutParams(layoutParams);
                }
            });
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String nickName = prefs.getString("NICKNAME",null);
        mTextView.setText(nickName);
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
