package com.damian.dkim.secretbucks.utility;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.multidex.MultiDex;

import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.model.Drink;
import com.kobakei.ratethisapp.RateThisApp;

import net.danlew.android.joda.JodaTimeAndroid;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by dkim on 11/11/2016.
 */

public class MyApplication extends Application {

    private static MyApplication singleton;
    public Drink drink;
    public boolean displayAd;

    public static MyApplication getInstance(){
        return singleton;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        MultiDex.install(this);
        super.onCreate();
        singleton = this;

        //display ad or not
        displayAd = true;

        //app rate dialog initialization
        RateThisApp.Config config = new RateThisApp.Config(3, 3);
        config.setUrl("https://play.google.com/store/apps/details?id=com.damian.dkim.secretbucks");
        RateThisApp.init(config);

        //JodaTime init
        JodaTimeAndroid.init(this);

        //Calligraphy init
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("Freight Sans Medium.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        // register to be informed of activities starting up
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity activity,
                                          Bundle savedInstanceState) {

                // new activity created; force its orientation to portrait
                activity.setRequestedOrientation(
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }

        });
    }

    public Drink getDrink() {
        return drink;
    }

    public boolean isDisplayAd() { return displayAd; }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }
}
