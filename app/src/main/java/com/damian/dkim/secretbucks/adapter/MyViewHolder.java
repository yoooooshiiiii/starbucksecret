package com.damian.dkim.secretbucks.adapter;

/**
 * Created by dkim on 11/3/2016.
 */


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.damian.dkim.secretbucks.R;

public class MyViewHolder extends RecyclerView.ViewHolder {

    //for DrinkAdapter
    public TextView drinkName, headerTitle, drinkComment, drinkLike, navMenu, navHeader, navSub, listName, listLike, listComment;
    public ImageView drinkImage, commentImage, menubg, commentUploadedImage, listImage, navIcon;
    public CardView cardLayout, navClick, menuClick;
    public RelativeLayout listLayout;

    //for CommentAdapter
    public TextView comment;
    public TextView timeStamp;

    public TextView menu;

    public TextView updateTime;
    public ImageView menuIcon;

    public MyViewHolder(View v) {
        super(v);
        //for DrinkAdapter
        cardLayout = (CardView) v.findViewById(R.id.card_view);
        drinkName = (TextView)v.findViewById(R.id.title);
        drinkLike = (TextView)v.findViewById(R.id.count);
        drinkImage = (ImageView)v.findViewById(R.id.thumbnail);
        drinkComment = (TextView)v.findViewById(R.id.tv_commentCount);

        //for commentAdapter
        comment = (TextView)v.findViewById(R.id.tv_comment2);
        timeStamp = (TextView)v.findViewById(R.id.tv_timestamp);
        commentImage = (ImageView) v.findViewById(R.id.iv_nickname);
        commentUploadedImage = (ImageView) v.findViewById(R.id.iv_comment);

        //for menuAdapter
        menu = (TextView)v.findViewById(R.id.tv_menu);
        menuClick = (CardView)v.findViewById(R.id.ll_menu);

        //for nav menu
        navMenu = (TextView) v.findViewById(R.id.tv_navmenu);
        navClick = (CardView) v.findViewById(R.id.cv_nav);
        navIcon = (ImageView) v.findViewById(R.id.iv_navicon);

        //for list
        listName = (TextView)v.findViewById(R.id.tv_listname);
        listLike = (TextView)v.findViewById(R.id.tv_count);
        listComment = (TextView)v.findViewById(R.id.tv_commentCount2);
        listImage = (ImageView)v.findViewById(R.id.iv_listimage);
        listLayout = (RelativeLayout) v.findViewById(R.id.rl_view);
    }
}