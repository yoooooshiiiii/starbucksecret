package com.damian.dkim.secretbucks.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by dkim on 11/9/2016.
 */

public class Comment{
    String _id;
    Date timeStamp;
    String userId;
    String userName;
    String comment;
    String url;

    public Comment() {
        ArrayList<Integer> i = new ArrayList<>();
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
