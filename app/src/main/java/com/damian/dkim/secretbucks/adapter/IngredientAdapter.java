package com.damian.dkim.secretbucks.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.damian.dkim.secretbucks.R;
import com.damian.dkim.secretbucks.model.Detail;

import java.util.List;

/**
 * Created by dkim on 6/7/2017.
 */

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.MyViewHolder>{
    List<Detail> detail;
    private Context mContext;

    public IngredientAdapter(Context mContext, List<Detail> detail) {
        this.mContext = mContext;
        this.detail = detail;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_detail, parent, false);
        IngredientAdapter.MyViewHolder rv = new IngredientAdapter.MyViewHolder(v);
        return rv;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String name = detail.get(position).getIngredient().getItem();
        Double serving = detail.get(position).getIngredient().getServing();
        String unit = detail.get(position).getIngredient().getUnit();
        String str;
        if(serving % 1 == 0){
            int servingWhole = serving.intValue();
            str = name + " " + servingWhole + " " + unit;
        } else {
            str = name + " " + serving + " " + unit;
        }
        holder.detail.setText(str);
    }

    @Override
    public int getItemCount() {
        return detail.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView detail;

        public MyViewHolder(View v){
            super(v);
            detail = (TextView) v.findViewById(R.id.tv_detail);
        }

    }
}
