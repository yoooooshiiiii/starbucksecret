//package com.example.dkim.secretmacchiatos.ui.fragments;
//
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.provider.Settings;
//import android.support.v4.app.Fragment;
//import android.text.method.LinkMovementMethod;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.example.dkim.secretmacchiatos.BuildConfig;
//import com.example.dkim.secretmacchiatos.R;
//
//import static com.facebook.FacebookSdk.getApplicationContext;
////import com.example.dkim.secretmacchiatos.ui.activities.PreRegisterAcitivty;
//
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class UserFragment extends Fragment {
//    private Context mContext;
//    private RelativeLayout mLayout;
//    private TextView mTextView, mTextView2, mTextView3;
//    private SharedPreferences prefs;
//    private String nickName, uid;
//    private Typeface face;
//    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
//
//    public UserFragment() {
//        // Required empty public constructor
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//
////        mTextView.setOnClickListener(v -> {
////            Intent intent = new Intent(getActivity(), PreRegisterAcitivty.class);
////            startActivity(intent);
////        });
//        face = Typeface.createFromAsset(getActivity().getApplicationContext().getAssets(), "Nunito-SemiBold.ttf");
//        mTextView.setTextSize(18);
//        mTextView.setTypeface(face);
//        mTextView.setMovementMethod(LinkMovementMethod.getInstance());
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        //make sure activity is created
//        super.onActivityCreated(savedInstanceState);
//        // Inflate the layout for this fragment
//
//        View view = inflater.inflate(R.layout.fragment_user, container, false);
//        mTextView = (TextView) view.findViewById(R.id.tv_nickname);
//        mTextView2 = (TextView) view.findViewById(R.id.tv_uid);
//        mTextView3 = (TextView) view.findViewById(R.id.tv_bid);
//
//        nickName = "Guest";
//        uid = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
//
//        mTextView.setText(nickName);
//        mTextView2.setText(uid);
//        mTextView3.setText(BuildConfig.VERSION_NAME);
//        return view;
//    }
//}
