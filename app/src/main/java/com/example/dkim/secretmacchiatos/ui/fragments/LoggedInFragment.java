//package com.example.dkim.secretmacchiatos.ui.fragments;
//
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.graphics.Rect;
//import android.os.Bundle;
//import android.preference.PreferenceManager;
//import android.support.design.widget.Snackbar;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.DefaultItemAnimator;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.DisplayMetrics;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.amulyakhare.textdrawable.TextDrawable;
//import com.amulyakhare.textdrawable.util.ColorGenerator;
//import com.example.dkim.secretmacchiatos.R;
//import com.example.dkim.secretmacchiatos.adapter.DrinkAdapter;
//import com.example.dkim.secretmacchiatos.model.Drink;
//import com.example.dkim.secretmacchiatos.model.User;
//import com.example.dkim.secretmacchiatos.utility.Constants;
//import com.example.dkim.secretmacchiatos.utility.RetrofitInterface;
//import com.example.dkim.secretmacchiatos.utility.ServiceGenerator;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class LoggedInFragment extends Fragment {
//    SharedPreferences prefs;
//    TextView name, email;
//    RetrofitInterface API;
//    ImageView mImageView, mImageView2;
//    RecyclerView.LayoutManager mLayoutManager;
//    RecyclerView recyclerView;
//    DrinkAdapter adapter;
//    List<Drink> drinkList;
//    String userId;
//    LinearLayout emptyView;
//
//    public LoggedInFragment() {
//        // Required empty public constructor
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        getLikedDrink();
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
//        userId = prefs.getString("ID", "");
//        getProfile(userId);
////n
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View v = inflater.inflate(R.layout.fragment_logged_in, container, false);
//        API = ServiceGenerator.createService(RetrofitInterface.class, Constants.TOKEN);
////        nickname = (TextView) v.findViewById(R.id.tv_nickname);
//        name = (TextView) v.findViewById(R.id.tv_name);
//        email = (TextView) v.findViewById(R.id.tv_email);
//        mImageView = (ImageView) v.findViewById(R.id.iv_profile);
//        mImageView2 = (ImageView) v.findViewById(R.id.iv_social);
////        timeStamp = (TextView) v.findViewById(R.id.tv_timestamp);
////        signout = (TextView) v.findViewById(R.id.tv_signout);
//
//        emptyView = (LinearLayout) v.findViewById(R.id.ll_empty);
//
//        //set two column grid
//        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//
//        //set recyclerView for this fragment
//        recyclerView = (RecyclerView) v.findViewById(R.id.rv_vote);
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new LoggedInFragment.SpacesItemDecoration(dpToPx(getActivity(), 8)));
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//
//        drinkList = new ArrayList<>();
//        adapter = new DrinkAdapter(getActivity(), drinkList);
//        recyclerView.setAdapter(adapter);
//        recyclerView.setVisibility(View.GONE);
//
//        return v;
//    }
//
//    private void getProfile(String userId) {
//        if (!userId.equals("") || userId != null) {
//            Call<User> call = API.getProfile(userId);
//            call.enqueue(new Callback<User>() {
//                @Override
//                public void onResponse(Call<User> call, Response<User> response) {
//                    if (response.body() != null && response.isSuccessful()) {
//                        String name;
//                        String email;
//                        switch (accountType(response.body())) {
//                            case "Local":
//                                name = response.body().getLocal().getName();
//                                email = response.body().getLocal().getEmail();
//                                mImageView2.setImageResource(R.drawable.email);
//                                break;
//                            case "Facebook":
//                                name = response.body().getFacebook().getName();
//                                email = response.body().getFacebook().getEmail();
//                                mImageView2.setImageResource(R.drawable.facebook_box);
//                                break;
//                            case "Google":
//                                name = response.body().getGoogle().getName();
//                                email = response.body().getGoogle().getEmail();
//                                mImageView2.setImageResource(R.drawable.google_plus_box);
//                                break;
//                            default:
//                                name = "";
//                                email = "";
//                                break;
//                        }
//                        setupProfile(name, email);
//                    } else {
//                        Toast.makeText(getActivity().getApplication(), "Oops! Something went wrong.", Toast.LENGTH_SHORT).show();
//                        getProfile(userId);
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<User> call, Throwable t) {
//                    getProfile(userId);
//                }
//            });
//        }
//    }
//    private void getLikedDrink() {
//        Call<List<Drink>> call = API.getLikedDrink(userId);
//        call.enqueue(new Callback<List<Drink>>() {
//            @Override
//            public void onResponse(Call<List<Drink>> call, Response<List<Drink>> response) {
//                if (response.body() != null) {
//                    drinkList = response.body();
//                    if (drinkList.isEmpty() || drinkList == null){
//                        recyclerView.setVisibility(View.GONE);
//                        emptyView.setVisibility(View.VISIBLE);
//                    } else {
//                        recyclerView.setVisibility(View.VISIBLE);
//                        emptyView.setVisibility(View.GONE);
//                    }
//                    adapter.update(drinkList);
//                    adapter.notifyDataSetChanged();
//                } else {
//                    Snackbar snackbar = Snackbar
//                            .make(getView(), "Server is offline, please try again!", Snackbar.LENGTH_INDEFINITE)
//                            .setAction("RETRY", view -> getLikedDrink());
//                    snackbar.show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<Drink>> call, Throwable t) {
//                getLikedDrink();
//            }
//        });
//    }
//
//    private String accountType(User user) {
//        if (user.getLocal() != null) {
//            return "Local";
//        } else if (user.getFacebook() != null) {
//            return "Facebook";
//        } else if (user.getGoogle() != null) {
//            return "Google";
//        } else
//            return "";
//    }
//
//    private void setupProfile(String name, String email) {
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putString("NICKNAME", name);
//        editor.commit();
//        this.name.setText(name);
//        this.email.setText(email);
//        ColorGenerator generator = ColorGenerator.MATERIAL;
//        int color = generator.getColor(prefs.getString("ID", ""));
//        TextDrawable drawable = TextDrawable.builder()
//                .buildRound(name.substring(0,1), color);
//        mImageView.setImageDrawable(drawable);
//    }
//
//
//    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
//
//        private int halfSpace;
//
//        public SpacesItemDecoration(int space) {
//            this.halfSpace = space / 2;
//        }
//
//        @Override
//        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//            // first item with no padding
//            if (parent.getPaddingLeft() != halfSpace) {
//                parent.setPadding(halfSpace, halfSpace, halfSpace, halfSpace);
//                parent.setClipToPadding(false);
//            }
//            outRect.top = 0;
//            outRect.bottom = 0;
//            outRect.left = halfSpace;
//            outRect.right = halfSpace;
//        }
//    }
//
//    public static int dpToPx(Context context, int dp) {
//        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
//        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
//        return px;
//    }
//}
