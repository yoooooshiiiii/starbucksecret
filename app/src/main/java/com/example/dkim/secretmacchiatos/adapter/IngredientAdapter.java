package com.example.dkim.secretmacchiatos.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.dkim.secretmacchiatos.R;
import com.example.dkim.secretmacchiatos.model.Detail;
import com.example.dkim.secretmacchiatos.model.Drink;

import java.util.List;

/**
 * Created by dkim on 6/7/2017.
 */

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.MyViewHolder>{
    Drink drink;
    List<Detail> detail;
    private Context mContext;

    public IngredientAdapter(Context mContext, List<Detail> detail) {
        this.mContext = mContext;
        this.detail = detail;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_detail, parent, false);
        IngredientAdapter.MyViewHolder rv = new IngredientAdapter.MyViewHolder(v);
        return rv;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String str = detail.get(position).getIngredient().getItem() + " " + detail.get(position).getIngredient().getServing() + " " + detail.get(position).getIngredient().getUnit();
        holder.detail.setText(str);
    }

    @Override
    public int getItemCount() {
        return detail.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView detail;

        public MyViewHolder(View v){
            super(v);
            detail = (TextView) v.findViewById(R.id.tv_detail);
        }

    }
}
