package com.example.dkim.secretmacchiatos.ui.fragments;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.dkim.secretmacchiatos.R;
import com.example.dkim.secretmacchiatos.adapter.DrinkAdapter;
import com.example.dkim.secretmacchiatos.model.Drink;
import com.example.dkim.secretmacchiatos.ui.activities.DetailActivity;
import com.example.dkim.secretmacchiatos.utility.Constants;
import com.example.dkim.secretmacchiatos.utility.MyApplication;
import com.example.dkim.secretmacchiatos.utility.RetrofitInterface;
import com.example.dkim.secretmacchiatos.utility.ServiceGenerator;
import com.github.clans.fab.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private View view;
    private RecyclerView recyclerView, recyclerView2;
    public DrinkAdapter adapter, adapter2;
    private List<Drink> drinkList, drinkList2;
    private LinearLayoutManager mLayoutManager, mLayoutManager2;
    private LinearLayout progressScreen;
    private Typeface face, face2;
    private TextView headerTitle, featuredTitle, featuredDesc, header, header2;
    private CardView cvFeatured;
    private boolean resume, load, load2, load3;
    private ProgressBar spinner;
    private TabLayout t;
    private RetrofitInterface DrinkAPI;
    private ImageView featuredImage;
    FloatingActionButton fab, fab2;

    public HomeFragment() {
        // Required empty public constructor
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if(isVisibleToUser && isResumed()){
//            FabUtil.showFab(fab);
//            FabUtil.hideFab(fab2);
//            System.out.println("homef frag");
//        }
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        //initialize fab
        // Initialize fab
//        fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
//        fab2 = (FloatingActionButton) getActivity().findViewById(R.id.fab2);
//        fab.setVisibility(View.VISIBLE);
//        fab2.setVisibility(View.GONE);
        //initialize spinner
        spinner = (ProgressBar) view.findViewById(R.id.progressBar);
        progressScreen = (LinearLayout) view.findViewById(R.id.progress_container);
        showPage(false);
        //set resume variable to true indicating fragment is newly created
        resume = true;
        //set typeface
        face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nunito-SemiBold.ttf");
        //initialize API interface to be used later
        DrinkAPI = ServiceGenerator.createService(RetrofitInterface.class, Constants.TOKEN);

        //set two column grid
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

        //set recyclerView for this fragment
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_popular);
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.addItemDecoration(new SpacesItemDecoration(dpToPx(getActivity(), 8)));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView2 = (RecyclerView) view.findViewById(R.id.rv_recent);
        recyclerView2.setLayoutManager(mLayoutManager2);
//        recyclerView2.addItemDecoration(new SpacesItemDecoration(dpToPx(getActivity(), 8)));
        recyclerView2.setItemAnimator(new DefaultItemAnimator());

        //adapter initialize with current activity with header and set adapter
        drinkList = new ArrayList<>();
        drinkList2 = new ArrayList<>();
        adapter = new DrinkAdapter(getActivity(), drinkList);
        adapter2 = new DrinkAdapter(getActivity(), drinkList2);
        recyclerView.setAdapter(adapter);
        recyclerView2.setAdapter(adapter2);

        cvFeatured = (CardView) view.findViewById(R.id.cv_menu);
        featuredImage = (ImageView) view.findViewById(R.id.iv_featured);
        featuredTitle = (TextView) view.findViewById(R.id.tv_featuredTitle);
        featuredDesc = (TextView) view.findViewById(R.id.tv_featuredDesc);

        header = (TextView) view.findViewById(R.id.tv_header1);
        header2 = (TextView) view.findViewById(R.id.tv_more1);
        header.setTypeface(face);
        header2.setTypeface(face);

//        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
//            @Override
//            public int getSpanSize(int position) {
//                return adapter.isHeader(position) ? mLayoutManager.getSpanCount() : 1;
//            }
//        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this);
            ft.attach(this);
            ft.commit();
        } else {
            getPopular();
            getFeatured();
            getRecent();
            resume = false;
        }
    }

    private void getFeatured() {
        Call<Drink> call = DrinkAPI.getFeatured();
        call.enqueue(new Callback<Drink>() {
            @Override
            public void onResponse(Call<Drink> call, Response<Drink> response) {
                Picasso.with(getActivity()).load(response.body().getUrl()).fit().centerCrop().into(featuredImage, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        load = true;
                        showPage(true);
                    }

                    @Override
                    public void onError() {
                        getFeatured();
                    }
                });
                featuredTitle.setText(response.body().getName());
                featuredDesc.setText(response.body().getDesc());
                cvFeatured.setOnClickListener(view -> {
                    Intent i = new Intent(getActivity(), DetailActivity.class);
                    MyApplication.getInstance().setDrink(response.body());
                    getActivity().startActivity(i);
                });
            }

            @Override
            public void onFailure(Call<Drink> call, Throwable t) {
                getFeatured();
            }
        });
    }

    private void getPopular() {
        Call<List<Drink>> call = DrinkAPI.getPopular();
        call.enqueue(new Callback<List<Drink>>() {
            @Override
            public void onResponse(Call<List<Drink>> call, Response<List<Drink>> response) {
                if (response.body() != null) {
                    drinkList.addAll(response.body());
                    adapter.notifyDataSetChanged();
                    load2 = true;
                    showPage(true);
                } else {
                    Snackbar snackbar = Snackbar
                            .make(getView(), "Server is offline, please try again!", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", view -> getPopular());
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<List<Drink>> call, Throwable t) {
                getPopular();
            }
        });
    }

    private void getRecent() {
        Call<List<Drink>> call = DrinkAPI.getRecent();
        call.enqueue(new Callback<List<Drink>>() {
            @Override
            public void onResponse(Call<List<Drink>> call, Response<List<Drink>> response) {
                if (response.body() != null) {
                    drinkList2.addAll(response.body());
                    adapter2.notifyDataSetChanged();
                    load3 = true;
                    showPage(true);
                } else {
                    Snackbar snackbar = Snackbar
                            .make(getView(), "Server is offline, please try again!", Snackbar.LENGTH_INDEFINITE)
                            .setAction("RETRY", view -> getRecent());
                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<List<Drink>> call, Throwable t) {
                getRecent();
            }
        });
    }

    private void showPage(boolean ready) {
        if (ready == true && load == true && load2 == true && load3 == true) {
            progressScreen.setVisibility(View.GONE);
        }
        if (ready == false) {
            progressScreen.setVisibility(View.VISIBLE);
        }
    }

//    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
//
//        private int halfSpace;
//
//        public SpacesItemDecoration(int space) {
//            this.halfSpace = space / 2;
//        }
//
//        @Override
//        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//            // first item with no padding
//            if (parent.getPaddingLeft() != halfSpace) {
//                parent.setPadding(halfSpace, halfSpace, halfSpace, halfSpace);
//                parent.setClipToPadding(false);
//            }
//            outRect.top = 0;
//            outRect.bottom = 0;
//            outRect.left = halfSpace;
//            outRect.right = halfSpace;
//        }
//    }
//
//    public static int dpToPx(Context context, int dp) {
//        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
//        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
//        return px;
//    }
}
