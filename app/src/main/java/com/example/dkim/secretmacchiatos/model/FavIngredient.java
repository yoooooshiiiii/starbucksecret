package com.example.dkim.secretmacchiatos.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dkim on 6/8/2017.
 */

@DatabaseTable(tableName = "ingredient")
public class FavIngredient {

    public FavIngredient() {
    }

    public static final String ID_FIELD_NAME = "detail_id";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName=ID_FIELD_NAME)
    private Favorite favorite;

    @DatabaseField
    private String name;

    @DatabaseField
    private double serving;

    @DatabaseField
    private String unit;

    @DatabaseField
    private double calorie;


    public FavIngredient(Favorite favorite, String name, double serving, String unit, double calorie) {
        this.favorite = favorite;
        this.name = name;
        this.serving = serving;
        this.unit = unit;
        this.calorie = calorie;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Favorite getFavorite() {
        return favorite;
    }

    public void setDetail(Favorite favorite) {
        this.favorite = favorite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getServing() {
        return serving;
    }

    public void setServing(double serving) {
        this.serving = serving;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getCalorie() {
        return calorie;
    }

    public void setCalorie(double calorie) {
        this.calorie = calorie;
    }
}
