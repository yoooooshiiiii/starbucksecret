package com.example.dkim.secretmacchiatos.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bhargavms.dotloader.DotLoader;
import com.example.dkim.secretmacchiatos.R;
import com.example.dkim.secretmacchiatos.adapter.ListAdapter;
import com.example.dkim.secretmacchiatos.model.Drink;
import com.example.dkim.secretmacchiatos.utility.Constants;
import com.example.dkim.secretmacchiatos.utility.RetrofitInterface;
import com.example.dkim.secretmacchiatos.utility.ServiceGenerator;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchActivity extends AppCompatActivity {

    private List<Drink> drinkList;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    Retrofit retrofit;
    RetrofitInterface API;
    private DotLoader spinner;
    private ListAdapter adapter;
    private String title;
    private Intent myIntent;
    private LinearLayout progressScreen;
    CoordinatorLayout mCoordinateLayout;
    private LinearLayout mLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        myIntent = getIntent();
        title = myIntent.getStringExtra("Search");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);

        //loading page
        progressScreen = (LinearLayout) findViewById(R.id.progress_container);
        progressScreen.setVisibility(View.VISIBLE);
        spinner = (DotLoader) this.findViewById(R.id.text_dot_loader);

        //no result page
        mLinearLayout = (LinearLayout) findViewById(R.id.ll_noResult);
        mLinearLayout.setVisibility(View.GONE);

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-3940256099942544~3347511713");
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        API = ServiceGenerator.createService(RetrofitInterface.class, Constants.TOKEN);

        mRecyclerView = (RecyclerView) this.findViewById(R.id.rv_list);
        mLayoutManager = new LinearLayoutManager(SearchActivity.this);
        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .marginResId(R.dimen.menudivider_margin, R.dimen.menudivider_margin)
                .build());
        mRecyclerView.setLayoutManager(mLayoutManager);
        drinkList = new ArrayList<>();
        adapter = new ListAdapter(this, drinkList);
        mRecyclerView.setAdapter(adapter);
        getDrinkList();


    }

    private void getDrinkList() {
        Call<List<Drink>> call = API.getSearch(title);
        call.enqueue(new Callback<List<Drink>>() {
            @Override
            public void onResponse(Call<List<Drink>> call, Response<List<Drink>> response) {
                if (response.body() == null || response.body().size() == 0) {
                    mLinearLayout.setVisibility(View.VISIBLE);
                    progressScreen.setVisibility(View.GONE);
                } else {
                    drinkList = response.body();
                    adapter.update(drinkList);
                    adapter.notifyDataSetChanged();
                    progressScreen.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<List<Drink>> call, Throwable t) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
