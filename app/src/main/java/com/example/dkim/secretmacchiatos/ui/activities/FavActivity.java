package com.example.dkim.secretmacchiatos.ui.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dkim.secretmacchiatos.R;
import com.example.dkim.secretmacchiatos.adapter.IngredientAdapter;
import com.example.dkim.secretmacchiatos.db.DatabaseHelper;
import com.example.dkim.secretmacchiatos.model.Detail;
import com.example.dkim.secretmacchiatos.model.Drink;
import com.example.dkim.secretmacchiatos.model.FavIngredient;
import com.example.dkim.secretmacchiatos.model.Favorite;
import com.example.dkim.secretmacchiatos.model.Ingredient;
import com.example.dkim.secretmacchiatos.utility.Constants;
import com.example.dkim.secretmacchiatos.utility.MyApplication;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FavActivity extends AppCompatActivity {
    private net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout collapsingToolbarLayout = null;
    private Typeface face;
    private Drink drink;
    private ImageView mImageView, mImageView2;
    private TextView mTextView, mTextView2, mTextView3, mTextView4;
    private RecyclerView detailView;
    private LinearLayoutManager mLayoutManager;
    private IngredientAdapter adapter;
    private List<FavIngredient> IngredientList;
    private Ingredient ingredient2;
    private Detail detail;
    private DatabaseHelper databaseHelper = null;
    private double calorie;
    private int c = 0;
    CoordinatorLayout mCoordinateLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav);
        drink = ((MyApplication) getApplication()).getDrink();
        face = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito-SemiBold.ttf");
        mImageView = (ImageView) findViewById(R.id.iv_detail);
        mImageView2 = (ImageView) findViewById(R.id.iv_delete);
        mTextView = (TextView) findViewById(R.id.tv_desc);
        mTextView2 = (TextView) findViewById(R.id.tv_note);
        mTextView2.setText(drink.getNote());
        mTextView3 = (TextView) findViewById(R.id.tv_calorie);


        MobileAds.initialize(getApplicationContext(), "ca-app-pub-3940256099942544~3347511713");
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mCoordinateLayout = (CoordinatorLayout)findViewById(R.id.cl_content);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                layoutParams.setMargins(0, 0, 0, mAdView.getHeight());
                mCoordinateLayout.setLayoutParams(layoutParams);
            }
        });

        final Dao<FavIngredient, Integer> detailDao = getHelper().getDetailDao();
        final Dao<Favorite, Integer> favDao = getHelper().getFavDataDao();
        List<Favorite> favData = new ArrayList<>();
        List<Detail> detailList = new ArrayList<>();
        try {
            favData = favDao.queryBuilder().where().eq(Favorite.FIELD_NAME, drink.getName()).query();
            IngredientList = detailDao.queryBuilder().where().eq(FavIngredient.ID_FIELD_NAME, favData.get(0).getFav_id()).query();
            for (FavIngredient ingredient:IngredientList) {
                ingredient2 = new Ingredient(ingredient.getName(), ingredient.getServing(), ingredient.getUnit(), ingredient.getCalorie());
                detail = new Detail();
                detail.setIngredient(ingredient2);
                detailList.add(detail);
            }
            Log.d("test", detailList.toString());
        } catch (SQLException e) {
            e.printStackTrace();
            //if sql error occurs, initialize favList as new array list to avoid null pointer exception
            detailList = new ArrayList<>();
        }

        final int dataId = favData.get(0).getFav_id();

        for (Detail item:detailList){
            Log.d("test", String.valueOf(item.getIngredient().getCalorie()));
            calorie += item.getIngredient().getCalorie();
        }
        mTextView3.setText("Estimated Calorie: " + String.valueOf((int)calorie) + "kcal");
        mImageView2.setOnClickListener(v->{
            try {
                DeleteBuilder<Favorite, Integer> deleteBuilder = favDao.deleteBuilder();
                DeleteBuilder<FavIngredient, Integer> deleteBuilder1 = detailDao.deleteBuilder();
                deleteBuilder.where().eq(Favorite.FIELD_NAME, drink.getName());
                deleteBuilder.delete();

                deleteBuilder1.where().eq(FavIngredient.ID_FIELD_NAME, dataId);
                deleteBuilder1.delete();
                Toast.makeText(FavActivity.this,"Drink removed", Toast.LENGTH_LONG).show();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            onBackPressed();
        });

        //take care of recyclerview
        detailView = (RecyclerView) findViewById(R.id.rv_ingredient);
        mLayoutManager = new LinearLayoutManager(this);
        detailView.setLayoutManager(mLayoutManager);
        detailView.setItemAnimator(new DefaultItemAnimator());
        adapter = new IngredientAdapter(this, detailList);
        detailView.setAdapter(adapter);



        Picasso.with(this).load(new File(drink.getUrl())).placeholder(R.drawable.placeholder).fit().into(mImageView, new Callback() {
            @Override
            public void onSuccess() {
                c = dynamicToolbarColor();
                if (Build.VERSION.SDK_INT >= 21) {
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(c);
                } else {
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
            }

            @Override
            public void onError() {
            }
        });

        mTextView.setText(drink.getDesc());

        //initialize toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbarInit();

    }

    private void toolbarInit() {
        collapsingToolbarLayout = (net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(drink.getName());
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
        collapsingToolbarLayout.setCollapsedTitleTypeface(face);
        collapsingToolbarLayout.setExpandedTitleTypeface(face);
    }
    private int dynamicToolbarColor() {
        Bitmap bitmap = ((BitmapDrawable) mImageView.getDrawable()).getBitmap();
        Palette p = Palette.from(bitmap).generate();
        int c = p.getMutedColor(Color.parseColor(Constants.COLOR_PRIMARY));
        Palette.from(bitmap).generate(palette -> {
            collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(Color.parseColor(Constants.COLOR_PRIMARY)));
            collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(Color.parseColor(Constants.COLOR_PRIMARY)));
        });
        return c;
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(FavActivity.this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
}
