package com.example.dkim.secretmacchiatos.ui.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dkim.secretmacchiatos.R;
import com.example.dkim.secretmacchiatos.adapter.IngredientAdapter;
import com.example.dkim.secretmacchiatos.db.DatabaseHelper;
import com.example.dkim.secretmacchiatos.model.Detail;
import com.example.dkim.secretmacchiatos.model.Drink;
import com.example.dkim.secretmacchiatos.model.FavIngredient;
import com.example.dkim.secretmacchiatos.model.Favorite;
import com.example.dkim.secretmacchiatos.utility.Constants;
import com.example.dkim.secretmacchiatos.utility.MyApplication;
import com.example.dkim.secretmacchiatos.utility.RetrofitInterface;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class DetailActivity extends AppCompatActivity {

    private net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout collapsingToolbarLayout = null;
    private LinearLayout mLinearLayout, mLinearLayout2;
    private ImageView mImageView, mImageView2, mImageView3, mImageView4;
    private TextView mTextView, mTextView2, mTextView3, mTextView4, mTextView5;
    private int voteCount, commentCount;
    private SharedPreferences prefs;
    private int c = 0;
    private double calorie;
    private Drink drink;
    private Typeface face;
    private String imagePath;
    private DatabaseHelper databaseHelper = null;
    private RecyclerView detailView;
    private LinearLayoutManager mLayoutManager;
    private IngredientAdapter adapter;
    private List<Detail> itemList;
    private CoordinatorLayout mCoordinateLayout;
    final private static int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        //get drink (global obj)
        drink = ((MyApplication) getApplication()).getDrink();
        //initialize prefs
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //numbers for vote count and comment count

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-3940256099942544~3347511713");
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mCoordinateLayout = (CoordinatorLayout)findViewById(R.id.cl_content);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                layoutParams.setMargins(0, 0, 0, mAdView.getHeight());
                mCoordinateLayout.setLayoutParams(layoutParams);
            }
        });

        face = Typeface.createFromAsset(getApplicationContext().getAssets(), "Nunito-SemiBold.ttf");
        //clickable linear layouts
        mLinearLayout = (LinearLayout) findViewById(R.id.ll_vote);
        mLinearLayout2 = (LinearLayout) findViewById(R.id.ll_cmt);

        //take care of recyclerview
        detailView = (RecyclerView) findViewById(R.id.rv_ingredient);
        mLayoutManager = new LinearLayoutManager(this);
        detailView.setLayoutManager(mLayoutManager);
        detailView.setItemAnimator(new DefaultItemAnimator());
        itemList = drink.getDetail();
        adapter = new IngredientAdapter(this, itemList);
        detailView.setAdapter(adapter);

        for (Detail detail:itemList){
            calorie += detail.getIngredient().getCalorie();
        }

        //take care of text
        mTextView = (TextView) findViewById(R.id.tv_voteCount);
        mTextView2 = (TextView) findViewById(R.id.tv_comment2);
        mTextView3 = (TextView) findViewById(R.id.tv_desc);
        mTextView4 = (TextView) findViewById(R.id.tv_note);
        mTextView4.setText(drink.getNote());
        mTextView5 = (TextView) findViewById(R.id.tv_calorie);
        mTextView5.setText("Estimated Calorie: " + String.valueOf((int)calorie) + "kcal");

        mTextView3.setText(drink.getDesc());
        mTextView3.setTypeface(face);

        //take cares of images
        mImageView = (ImageView) findViewById(R.id.iv_detail);
        mImageView2 = (ImageView) findViewById(R.id.iv_vote);
        mImageView3 = (ImageView) findViewById(R.id.iv_comment);


        mImageView4 = (ImageView) findViewById(R.id.iv_overflow);

        mImageView4.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(this, mImageView4);
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.menu_option1:
                        saveImagefromUrl(drink.getUrl());
                        return true;
                    case R.id.menu_option2:
                        return true;
                }
                return false;
            });
            MenuInflater inflater = popupMenu.getMenuInflater();
            inflater.inflate(R.menu.menu_drink, popupMenu.getMenu());
            popupMenu.show();
        });

        Picasso.with(this).load(drink.getUrl()).placeholder(R.drawable.placeholder).fit().centerCrop().into(mImageView, new Callback() {
            @Override
            public void onSuccess() {
                c = dynamicToolbarColor();
                if (Build.VERSION.SDK_INT >= 21) {
                    Window window = getWindow();
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(c);
                } else {
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                }
            }

            @Override
            public void onError() {
            }
        });

        //check voted or not
        if (voteCheck()) {
            mImageView2.setImageResource(R.drawable.ic_vote_yes);
        } else {
            mImageView2.setImageResource(R.drawable.ic_vote_no);
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbarInit();

    }

    private final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if(msg.arg1 == 1)
                Toast.makeText(getApplicationContext(),"Drink has been added to favorites", Toast.LENGTH_LONG).show();
            else if(msg.arg1 == 2)
                Toast.makeText(getApplicationContext(),"This drink is already your favorite", Toast.LENGTH_LONG).show();
        }
    };

    private void addtoFavorite(Drink drink) {

        Favorite favData = new Favorite();
        FavIngredient favDetail;
        Message msg = handler.obtainMessage();
        if (checkDuplicate(drink) == false) {
            favData.setFav_name(drink.getName());
            favData.setFav_image(imagePath);
            favData.setFav_description(drink.getDesc());
            favData.setFav_note(drink.getNote());
            try {
                final Dao<Favorite, Integer> favDao = getHelper().getFavDataDao();
                final Dao<FavIngredient, Integer> detailDao = getHelper().getDetailDao();
                favDao.create(favData);
                for (Detail i : drink.getDetail()){
                    favDetail = new FavIngredient(favData, i.getIngredient().getItem(), i.getIngredient().getServing(), i.getIngredient().getUnit(), i.getIngredient().getCalorie());
                    detailDao.create(favDetail);
                }
                msg.arg1 = 1;
                handler.sendMessage(msg);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            //if duplicate is found
            msg.arg1 = 2;
            handler.sendMessage(msg);
        }
    }

    private boolean checkDuplicate(Drink drink) {
        //true means duplicated
        final Dao<Favorite, Integer> favDao = getHelper().getFavDataDao();
        try {
            //SELECT COUNT (*) FROM FavData WHERE fav_name = drink.getname
            long numRow = favDao.queryBuilder()
                    .where()
                    .eq(Favorite.FIELD_NAME, drink.getName())
                    .countOf();
            //if result is 0, not duplicate
            if (numRow == 0) {
                return false;
            } else
            //if result is found, assume duplicate
                return true;
            //sql error
        } catch (SQLException e) {
            e.printStackTrace();
            return true;
        }
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private void saveImagefromUrl(String url) {
        if (checkReadPermission()) {
            Picasso.with(this).load(url)
                    .error(R.drawable.placeholder)
                    .into(target);
        } else {
            Toast.makeText(getApplicationContext(), "Permission required", Toast.LENGTH_SHORT).show();
        }

    }

    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
            new Thread(() -> {
                String NOMEDIA = ".nomedia";
                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/StarbucksSecret/" + System.currentTimeMillis() + ".jpg");
                File noMedia = new File(Environment.getExternalStorageDirectory().getPath() + "/StarbucksSecret/" + NOMEDIA);
                File directory = new File(Environment.getExternalStorageDirectory().getPath(), "StarbucksSecret");
                if (!directory.exists()){
                    directory.mkdirs();
                }
                try {
                    if (!noMedia.exists()){
                        noMedia.createNewFile();
                    }
                    file.createNewFile();
                    FileOutputStream ostream = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 75, ostream);
                    imagePath = file.getAbsolutePath();
                    Log.d("PATH", imagePath);
                    addtoFavorite(drink);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            if (placeHolderDrawable != null) {
            }
        }
    };

    private void toolbarInit() {
        collapsingToolbarLayout = (net.opacapp.multilinecollapsingtoolbar.CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(drink.getName());
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
        collapsingToolbarLayout.setCollapsedTitleTypeface(face);
        collapsingToolbarLayout.setExpandedTitleTypeface(face);
    }

    private void vote() {
        //Change icon instantly first and add vote
        mImageView2.setImageResource(R.drawable.ic_vote_yes);
        mTextView.setText(String.valueOf(voteCount + 1));
        //instantiating a retrofit object with baseURL
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //instantiating a Call object
        RetrofitInterface DrinkAPI = retrofit.create(RetrofitInterface.class);
        String userId = prefs.getString("ID", null);
        Call<Drink> call = DrinkAPI.putVote(drink.get_id(), userId);
        call.enqueue(new retrofit2.Callback<Drink>() {
            @Override
            public void onResponse(Call<Drink> call, Response<Drink> response) {
                drink.getVote().add(prefs.getString("ID", null));
            }

            @Override
            public void onFailure(Call<Drink> call, Throwable t) {
                Toast.makeText(getApplication(), "Oops! Something went wrong.", Toast.LENGTH_SHORT).show();
                mImageView2.setImageResource(R.drawable.ic_vote_no);
            }
        });
    }


    private boolean voteCheck() {
        return drink.getVote().contains(prefs.getString("ID", null));
    }

    private int dynamicToolbarColor() {
        Bitmap bitmap = ((BitmapDrawable) mImageView.getDrawable()).getBitmap();
        Palette p = Palette.from(bitmap).generate();
        int c = p.getMutedColor(Color.parseColor(Constants.COLOR_PRIMARY));
        Palette.from(bitmap).generate(palette -> {
            collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(Color.parseColor(Constants.COLOR_PRIMARY)));
            collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(Color.parseColor(Constants.COLOR_PRIMARY)));
        });
        return c;
    }

    //
//
    private int dp2px(int dp) {
        float scale = getResources().getDisplayMetrics().density;
        int pixels = (int) (dp * scale + 0.5f);
        return pixels;
    }

    @Override
    protected void onResume() {
        super.onResume();
        drink = ((MyApplication) getApplication()).getDrink();
        voteCount = drink.getVote().size();
        commentCount = drink.getComment().size();
        mTextView.setText(String.valueOf(voteCount));
        mTextView2.setText(String.valueOf(commentCount));

        mLinearLayout.setOnClickListener(v -> {
            if (voteCheck()) {
                Toast.makeText(this, "You can only like this drink once :)", Toast.LENGTH_SHORT).show();
            } else {
                vote();
            }
        });
        mLinearLayout2.setOnClickListener(v -> {
            Intent i = new Intent(this, CommentActivity.class);
            i.putExtra("color", c);
            this.startActivity(i);
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public boolean checkReadPermission(){
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                return false;
            }
        } else {
            return true;
        }
    }
}