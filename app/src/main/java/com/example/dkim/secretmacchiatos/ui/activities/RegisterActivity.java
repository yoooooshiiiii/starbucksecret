//package com.example.dkim.secretmacchiatos.ui.activities;
//
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.preference.PreferenceManager;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.view.MenuItem;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.example.dkim.secretmacchiatos.R;
//import com.example.dkim.secretmacchiatos.model.User;
//import com.example.dkim.secretmacchiatos.utility.Constants;
//import com.example.dkim.secretmacchiatos.utility.RetrofitInterface;
//import com.example.dkim.secretmacchiatos.utility.ServiceGenerator;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class RegisterActivity extends AppCompatActivity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_register);
//        EditText editName, editEmail, editPassword;
//        Button btnRegister;
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Registration");
//
//        editName = (EditText) findViewById(R.id.et_name);
//        editEmail = (EditText) findViewById(R.id.et_email);
//        editPassword = (EditText) findViewById(R.id.et_password);
//        btnRegister = (Button) findViewById(R.id.btn_register);
//
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//        RetrofitInterface API = ServiceGenerator.createService(RetrofitInterface.class, Constants.TOKEN);
//
//        btnRegister.setOnClickListener(v -> {
//            Call<User> call = API.createUser(editName.getText().toString(), editEmail.getText().toString().trim(), editPassword.getText().toString().trim());
//            call.enqueue(new Callback<User>() {
//                @Override
//                public void onResponse(Call<User> call, Response<User> response) {
//                    System.out.println(response.body());
//                    if (response.body() != null) {
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putBoolean("STATUS", true);
//                        editor.commit();
//                        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
//                        startActivity(intent);
//                    }
//                    else if (response.body() == null) {
//                        Toast.makeText(getApplicationContext(), "Invalid username or password", Toast.LENGTH_SHORT).show();
//                    }
//                }
//                @Override
//                public void onFailure(Call<User> call, Throwable t) {
//                    System.out.println(t);
//                    Toast.makeText(getApplicationContext(), "Server is slow, please try again.", Toast.LENGTH_SHORT).show();
//                }
//            });
//        });
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//}
