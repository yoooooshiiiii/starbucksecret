package com.example.dkim.secretmacchiatos.db;

import android.content.Context;

import com.example.dkim.secretmacchiatos.model.Favorite;
import com.j256.ormlite.dao.Dao;

/**
 * Created by dkim on 10/11/2016.
 */

public class DatabaseManager {
    private static DatabaseManager instance;
    private static DatabaseHelper helper;

    static public void init(Context ctx) {
        if (null==instance) {
            instance = new DatabaseManager(ctx);
        }
    }

    private DatabaseManager(Context ctx) {
        helper = new DatabaseHelper(ctx);
    }
    static public DatabaseManager getInstance() {
        return instance;
    }
    public static Dao<Favorite, Integer> getFavDataDao() { return helper.getFavDataDao();
    }
}
