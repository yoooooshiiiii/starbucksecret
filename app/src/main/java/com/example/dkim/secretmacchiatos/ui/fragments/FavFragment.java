package com.example.dkim.secretmacchiatos.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.dkim.secretmacchiatos.R;
import com.example.dkim.secretmacchiatos.adapter.FavAdapter;
import com.example.dkim.secretmacchiatos.db.DatabaseHelper;
import com.example.dkim.secretmacchiatos.model.Favorite;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavFragment extends Fragment {
    private DatabaseHelper databaseHelper = null;
    private RecyclerView mRecyclerView;
    private LinearLayout mLinearLayout;
    private LinearLayoutManager mLayoutManager;
    private List<Favorite> favList;
    private FavAdapter adapter;
    private boolean resume;
    private int layoutHeight;
    private RelativeLayout mRelativeLayout;

    public FavFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fav, container, false);
        mLinearLayout = (LinearLayout) v.findViewById(R.id.ll_noResult);
        mLinearLayout.setVisibility(View.GONE);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_favlist);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                .marginResId(R.dimen.menudivider_margin, R.dimen.menudivider_margin)
                .build());
        mRecyclerView.setLayoutManager(mLayoutManager);

        resume = true;
        final Dao<Favorite, Integer> favDao = getHelper().getFavDataDao();
        try {
            favList = favDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            //if sql error occurs, initialize favList as new array list to avoid null pointer exception
            favList = new ArrayList<>();
        }

        adapter = new FavAdapter(getActivity(), favList);
        adapter.update(favList);
        mRecyclerView.setAdapter(adapter);
        if (favList.size() == 0) {
            mLinearLayout.setVisibility(View.VISIBLE);
        }
        return v;
    }

    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!resume) {
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this);
            ft.attach(this);
            ft.commit();
        } else {
            resume = false;
        }
    }


}
