//package com.example.dkim.secretmacchiatos.ui.activities;
//
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.graphics.drawable.GradientDrawable;
//import android.os.Bundle;
//import android.preference.PreferenceManager;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.util.Log;
//import android.view.MenuItem;
//import android.widget.Button;
//import android.widget.Toast;
//
//import com.example.dkim.secretmacchiatos.R;
//import com.example.dkim.secretmacchiatos.model.User;
//import com.example.dkim.secretmacchiatos.utility.Constants;
//import com.example.dkim.secretmacchiatos.utility.RetrofitInterface;
//import com.example.dkim.secretmacchiatos.utility.ServiceGenerator;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.GraphRequest;
//import com.facebook.login.LoginManager;
//import com.facebook.login.LoginResult;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.Arrays;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class PreRegisterAcitivty extends AppCompatActivity {
//    CallbackManager callbackManager;
//    Button btnFacebook, btnGoogle, btnEmail, btnSignin;
//    SharedPreferences prefs;
//    String id,email,name;
//    final RetrofitInterface API = ServiceGenerator.createService(RetrofitInterface.class, Constants.TOKEN);
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_pre_register_acitivty);
//
//        //initialize facebook SDK and callback manager
//        callbackManager = CallbackManager.Factory.create();
//
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Registration");
//
//        prefs = PreferenceManager.getDefaultSharedPreferences(this);
//
//        btnFacebook = (Button) findViewById(R.id.btn_facebook);
//        btnGoogle = (Button) findViewById(R.id.btn_google);
//        btnEmail = (Button) findViewById(R.id.btn_email);
//        btnSignin = (Button) findViewById(R.id.btn_signin);
//
//        Button[] btnArray = new Button[]{ btnFacebook, btnGoogle, btnEmail, btnSignin };
//        int[] colorArray = new int[]{ 0xFF3b5998,0xFFdd4b39,0xFF302c26,0xFF302c26 };
//        int i = 0;
//        for (Button btn: btnArray){
//            setButton(btn, 10, colorArray[i]);
//            i++;
//        }
//
//        // Callback registration
//        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), (object, response) -> {
//                    Log.d("JSON", "" + response.getJSONObject().toString());
//                    JSONObject json = response.getJSONObject();
//                    try {
//                        if (json != null) {
//                            id = json.getString("id");
//                            email = json.getString("email");
//                            name = json.getString("name");
//                            startFacebook(id, email, name);
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                });
//                Bundle parameters = new Bundle();
//                parameters.putString("fields", "id,name,email");
//                graphRequest.setParameters(parameters);
//                graphRequest.executeAsync();
//            }
//            @Override
//            public void onCancel() {
//            }
//            @Override
//            public void onError(FacebookException exception) {
//            }
//        });
//
//
//    btnFacebook.setOnClickListener(v -> {
//            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email"));
//        });
//        btnGoogle.setOnClickListener(v -> {
//            startGoogle();
//        });
//        btnEmail.setOnClickListener(v -> {
//            Intent intent = new Intent(this, RegisterActivity.class);
//            startActivity(intent);
//        });
//        btnSignin.setOnClickListener(v -> {
//
//        });
//    }
//
//    private void setButton(Button btn, int radius, int color){
//        GradientDrawable gd = new GradientDrawable();
//        gd.setCornerRadius(radius);
//        gd.setColor(color);
//        btn.setBackground(gd);
//    }
//    private void startFacebook(String id, String email, String name){
//        Call<User> call = API.registerFacebook(id, email, name);
//        call.enqueue(new Callback<User>() {
//            @Override
//            public void onResponse(Call<User> call, Response<User> response) {
//                if (response.body() != null && response.isSuccessful()) {
//                    Toast.makeText(getApplicationContext(), "Welcome! " + response.body().getFacebook().getName(), Toast.LENGTH_SHORT).show();
//                    SharedPreferences.Editor editor = prefs.edit();
//                    editor.putBoolean("STATUS", true);
//                    editor.putString("ID", response.body().get_id());
//                    System.out.println(response.body().get_id());
//                    editor.commit();
//                    Intent intent = new Intent(PreRegisterAcitivty.this, MainActivity.class);
//                    startActivity(intent);
//                } else {
//                    Toast.makeText(getApplicationContext(), "Sync Failed", Toast.LENGTH_SHORT).show();
//                }
//            }
//            @Override
//            public void onFailure(Call<User> call, Throwable t) {
//                System.out.println(t);
//                Toast.makeText(getApplicationContext(), "Server is slow, please try again.", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    private void startGoogle(){
//        Call<User> call = API.startGoogle();
//        call.enqueue(new Callback<User>() {
//            @Override
//            public void onResponse(Call<User> call, Response<User> response) {
//                System.out.println(response.body());
//                if (response.code() == 200) {
//                    Toast.makeText(getApplicationContext(), "Sync with Google Successful!", Toast.LENGTH_SHORT).show();
//                    SharedPreferences.Editor editor = prefs.edit();
//                    editor.putBoolean("Status", true);
//                    editor.putString("Nickname", response.body().getGoogle().getName());
//                    editor.commit();
//                    Intent intent = new Intent(PreRegisterAcitivty.this, MainActivity.class);
//                    startActivity(intent);
//                }
//                else if (response.code() == 401)
//                    Toast.makeText(getApplicationContext(), "Failed Syncing with Google", Toast.LENGTH_SHORT).show();
//            }
//            @Override
//            public void onFailure(Call<User> call, Throwable t) {
//                System.out.println(t);
//                Toast.makeText(getApplicationContext(), "Server is slow, please try again.", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data)
//    {
//        super.onActivityResult(requestCode, resultCode, data);
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//}
