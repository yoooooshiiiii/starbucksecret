package com.example.dkim.secretmacchiatos.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dkim.secretmacchiatos.R;
import com.example.dkim.secretmacchiatos.model.Drink;
import com.example.dkim.secretmacchiatos.ui.activities.DetailActivity;
import com.example.dkim.secretmacchiatos.utility.CircleTransform;
import com.example.dkim.secretmacchiatos.utility.MyApplication;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by dkim on 3/24/2017.
 */

public class ListAdapter extends RecyclerView.Adapter<MyViewHolder> {

    private Context mContext;
    private List<Drink> drinkList;
    private Drink drink;

    public ListAdapter(Context mContext, List<Drink> drinkList) {
        this.mContext = mContext;
        this.drinkList = drinkList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list, parent, false);
        MyViewHolder rv = new MyViewHolder(v);
        return rv;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        drink = drinkList.get(position);
        holder.listName.setText(drink.getName());
        holder.listLike.setText(String.valueOf(drink.getVote_count()));
        holder.listComment.setText(String.valueOf(drink.getComment().size()));
        Picasso.with(mContext).load(drink.getUrl()).transform(new CircleTransform()).placeholder(R.drawable.placeholder).fit().centerCrop().into(holder.listImage);
        holder.listLayout.setOnClickListener(v->{
            Intent i = new Intent(mContext, DetailActivity.class);
            MyApplication.getInstance().setDrink(drinkList.get(position));
            mContext.startActivity(i);
        });

    }

    @Override
    public int getItemCount() {
        return drinkList.size();
    }

    public void update(List<Drink> dl){
        drinkList = dl;
    }
}
