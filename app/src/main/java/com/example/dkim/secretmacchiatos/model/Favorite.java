package com.example.dkim.secretmacchiatos.model;

import android.util.Log;

import com.example.dkim.secretmacchiatos.db.DatabaseManager;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.sql.SQLException;

/**
 * Created by dkim on 6/6/2017.
 */

@DatabaseTable(tableName = "fav")
public class Favorite {

    public static final String FIELD_NAME = "_NAME";

    @DatabaseField(generatedId = true, canBeNull = false)
    private int fav_id;

    @DatabaseField(columnName = FIELD_NAME)
    private String fav_name;

    @DatabaseField
    private String fav_image;

    @DatabaseField
    private String fav_description;

    @DatabaseField
    private String fav_note;

    @ForeignCollectionField(eager = true)
    private ForeignCollection<FavIngredient> fav_ingredient;

    public Favorite() {
    }

    public int getFav_id() {
        return fav_id;
    }

    public void setFav_id(int fav_id) {
        this.fav_id = fav_id;
    }

    public String getFav_name() {
        return fav_name;
    }

    public void setFav_name(String fav_name) {
        this.fav_name = fav_name;
    }

    public String getFav_image() {
        return fav_image;
    }

    public void setFav_image(String fav_image) {
        this.fav_image = fav_image;
    }

    public String getFav_description() {
        return fav_description;
    }

    public void setFav_description(String fav_description) {
        this.fav_description = fav_description;
    }

    public ForeignCollection<FavIngredient> getFav_ingredient() {
        return fav_ingredient;
    }

    public void setFav_ingredient(ForeignCollection<FavIngredient> fav_ingredient) {
        this.fav_ingredient = fav_ingredient;
    }

    public String getFav_note() {
        return fav_note;
    }

    public void setFav_note(String fav_note) {
        this.fav_note = fav_note;
    }

    static public Favorite getbyId(int fav_id){
        Favorite fav = null;
        try {
            fav = DatabaseManager.getFavDataDao().queryForId(fav_id);
        } catch (SQLException e) {
            Log.e(Favorite.class.getSimpleName(), "getById", e);
        }
        return fav;
    }
}
