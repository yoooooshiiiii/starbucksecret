package com.example.dkim.secretmacchiatos.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dkim.secretmacchiatos.R;
import com.example.dkim.secretmacchiatos.ui.activities.ListActivity;

import java.util.List;

/**
 * Created by dkim on 11/20/2016.
 */

public class MenuAdapter extends RecyclerView.Adapter<MyViewHolder> {
    private Context ctx;
    private List<String> menu;
    private List<Integer> menuIcon;
    private Typeface face;
    public static final int ITEM = 0;
    public static final int FULLSIZE = 1;
    private SharedPreferences prefs;
    private int layoutHeight;

    public MenuAdapter(Context ctx, List<String> menu, List<Integer> menuIcon, int layoutHeight) {
        this.ctx = ctx;
        this.menu = menu;
        this.menuIcon = menuIcon;
        this.layoutHeight = layoutHeight;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_menu, parent, false);
        ViewGroup.LayoutParams params = v.getLayoutParams();
        params.height = layoutHeight/6;
        v.setLayoutParams(params);
        MyViewHolder rv = new MyViewHolder(v);
        return rv;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.menu.setText(menu.get(position));
        holder.menuIcon.setImageResource(menuIcon.get(position));
        Intent i = new Intent(ctx, ListActivity.class);
        switch (position) {
            case 0:
                holder.menuClick.setBackgroundColor(Color.parseColor("#C6CCA5"));
                i.putExtra("Category","Frappuccinos");
                break;
            case 1:
                holder.menuClick.setBackgroundColor(Color.parseColor("#8AB8A8"));
                i.putExtra("Category","Hot Drinks");
                break;
            case 2:
                holder.menuClick.setBackgroundColor(Color.parseColor("#6B9997"));
                i.putExtra("Category","Cold Drinks");
                break;
            case 3:
                holder.menuClick.setBackgroundColor(Color.parseColor("#54787D"));
                i.putExtra("Category","Teas");
                break;
            default:
                break;
        }
        holder.menuClick.setOnClickListener(v -> {
            ctx.startActivity(i);
        });

    }

    @Override
    public int getItemViewType(int position) {
        int viewType;
        if (position == menu.size() - 1) {
            viewType = FULLSIZE;
        } else {
            viewType = ITEM;
        }
        return viewType;
    }

    @Override
    public int getItemCount() {
        return menu.size();
    }
}