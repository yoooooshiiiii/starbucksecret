package com.example.dkim.secretmacchiatos.model;

import java.util.List;

/**
 * Created by dkim on 10/28/2016.
 */

public class Drink {

    private String _id;
    private String name;
    private String url;
    private String category;
    private int vote_count;
    private List<String> vote;
    private String desc;
    private String note;
    private List<Comment> comment;
    private List<Detail> detail;


    public Drink() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public List<String> getVote() {
        return vote;
    }

    public void setVote(List<String> vote) {
        this.vote = vote;
    }

    public List<Comment> getComment() {
        return comment;
    }

    public void setComment(List<Comment> comment) {
        this.comment = comment;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


    public List<Detail> getDetail() {
        return detail;
    }

    public void setDetail(List<Detail> detail) {
        this.detail = detail;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}




