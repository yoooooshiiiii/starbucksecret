package com.example.dkim.secretmacchiatos.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dkim.secretmacchiatos.R;
import com.example.dkim.secretmacchiatos.db.DatabaseHelper;
import com.example.dkim.secretmacchiatos.model.Drink;
import com.example.dkim.secretmacchiatos.model.Favorite;
import com.example.dkim.secretmacchiatos.ui.activities.FavActivity;
import com.example.dkim.secretmacchiatos.utility.CircleTransform;
import com.example.dkim.secretmacchiatos.utility.MyApplication;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * Created by dkim on 6/6/2017.
 */

public class FavAdapter extends RecyclerView.Adapter<FavAdapter.MyViewHolder> {

    private Context mContext;
    private List<Favorite> favList;
    private Favorite fav;
    private DatabaseHelper databaseHelper = null;

    public FavAdapter(Context mContext, List<Favorite> drinkList) {
        this.mContext = mContext;
        this.favList = drinkList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_favlist, parent, false);
        MyViewHolder rv = new MyViewHolder(v);
        return rv;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        fav = favList.get(position);
        holder.favName.setText((fav.getFav_name()));
        Picasso.with(mContext).load(new File(fav.getFav_image())).transform(new CircleTransform()).placeholder(R.drawable.placeholder).fit().centerCrop().into(holder.favImage);
        holder.favClick.setOnClickListener(v->{
            Drink drink2 = new Drink();
            drink2.setName(favList.get(position).getFav_name());
            drink2.setUrl(favList.get(position).getFav_image());
            drink2.setDesc(favList.get(position).getFav_description());
            drink2.setNote(favList.get(position).getFav_note());
            MyApplication.getInstance().setDrink(drink2);
            Intent i = new Intent(mContext, FavActivity.class);
            mContext.startActivity(i);
        });
    }


    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(mContext, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public int getItemCount() {
        return favList.size();
    }

    public void update(List<Favorite> dl){
        favList = dl;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView favName;
        RelativeLayout favClick;
        ImageView favImage, favDelete;

        public MyViewHolder(View v){
            super(v);
            itemView.setOnClickListener(this);
            favName = (TextView) v.findViewById(R.id.tv_favname);
            favClick = (RelativeLayout) v.findViewById(R.id.rl_view);
            favImage = (ImageView) v.findViewById(R.id.iv_favimage);
            favDelete = (ImageView) v.findViewById(R.id.iv_delete);
        }

        @Override
        public void onClick(View v) {

        }
    }

}
