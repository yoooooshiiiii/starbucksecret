package com.example.dkim.secretmacchiatos.utility;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.view.animation.Animation;

import com.example.dkim.secretmacchiatos.R;
import com.github.clans.fab.FloatingActionButton;

/**
 * Created by dkim on 12/4/2016.
 */

public class FabUtil {
    public static int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    public static void hideFab(final FloatingActionButton fab) {
        fab.hide(true);
        fab.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fab.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    public static void showFab(final FloatingActionButton fab) {
        fab.setVisibility(View.VISIBLE);
        fab.show(true);
    }
}
